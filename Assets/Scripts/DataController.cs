﻿using System.Collections;
using System.IO;
using UnityEngine;

[System.Serializable]
public class DataController : MonoBehaviour
{
    public JsonData data = null;

    public JsonData CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<JsonData>(jsonString);
    }

    public JsonData GetData()
    {
        return data;
    }

    private string file = null;
    private string url = "https://hololens.perho.fi/wp-content/uploads/data.txt";

    //IEnumerator DownloadData()
    //{
    //    UIController.instance.GetNetworkStatusPanel().GetComponent<NetworkStatusPanel>().UpdateState(NetworkStates.CONNECTING);
    //    WWW www = new WWW(url);
    //    yield return www;
    //    if (www.error != null)
    //    {
    //        UIController.instance.GetNetworkStatusPanel().GetComponent<NetworkStatusPanel>().UpdateState(NetworkStates.OFFLINE);
    //    } else if (www.text.Contains("options"))
    //    {
    //        UIController.instance.GetNetworkStatusPanel().GetComponent<NetworkStatusPanel>().UpdateState(NetworkStates.ONLINE);
    //    } else
    //    {
    //        UIController.instance.GetNetworkStatusPanel().GetComponent<NetworkStatusPanel>().UpdateState(NetworkStates.OFFLINE);
    //    }
    //    // Check www is right
    //    this.file = www.text;
    //    SetData();
    //}

    private void SetData()
    {
        //if (this.file != null && this.file.Length > 0)
        //{
        //    data = CreateFromJSON(this.file);
        //    PlayerPrefs.SetString("data", this.file);
        //}
        if (PlayerPrefs.GetString("data") != null && PlayerPrefs.GetString("data").Length > 0)
        {
            data = CreateFromJSON(PlayerPrefs.GetString("data"));
        }
        else
        {
            TextAsset json = Resources.Load<TextAsset>("data");
            data = CreateFromJSON(json.text);
        }
        // when data is set
        UIController.instance.InitUI(data.options);
    }

    private void Start()
    {
        ResetData();
    }

    public void ResetData()
    {
        //StartCoroutine(DownloadData());

        SetData();
    }
}