﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuControl : MonoBehaviour {
    public GameObject MenuButtonPrefab;
    private int LastButtonPressedIndex = -1;
    private Transform MenuButtonListMain;
    private Transform MenuButtonListSub;

    public void ShowMainMenu()
    {
        LastButtonPressedIndex = -1;
        if (MenuButtonListMain == null)
            MenuButtonListMain = this.transform.Find("ListContainerMain");
        if (MenuButtonListSub != null)
            MenuButtonListSub.gameObject.SetActive(false);


        UIController.instance.GetDropdownPanel().SetActive(false);
        UIController.instance.GetContentContainer().SetActive(false);
        MenuButtonListMain.gameObject.SetActive(true);
        ResetButtonColors();
    }

    public void PopulateMenuButtons(List<Option> menu)
    {
        ShowMainMenu();
        foreach (Transform child in MenuButtonListMain)
        {
            if (child.gameObject.tag == "ScriptGeneratedButton")
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        AddButtonsToMenuList(MenuButtonListMain, menu);
    }

    private void PopulateSubMenuButtons(List<SubOption> options)
    {
        if (MenuButtonListMain == null)
            MenuButtonListMain = this.transform.Find("ListContainerMain");
        if (MenuButtonListSub == null)
            MenuButtonListSub = this.transform.Find("ListContainerSub");


        //ScriptGeneratedButton remove
        foreach (Transform child in MenuButtonListSub)
        {
            if (child.gameObject.tag == "ScriptGeneratedButton")
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        LastButtonPressedIndex = -1;
        AddButtonsToMenuList(MenuButtonListSub, options);
        MenuButtonListMain.gameObject.SetActive(false);
        MenuButtonListSub.gameObject.SetActive(true);
    }


    private void AddButtonsToMenuList(Transform target, dynamic menu)
    {
        for (int i = 0; i < menu.Count; i++)
        {
            // add check for supported buttons, if unsupported, continue
            GameObject newButton = Instantiate(MenuButtonPrefab, Vector3.zero, transform.rotation) as GameObject;

            newButton.transform.SetParent(target);
            newButton.transform.localScale = new Vector3(1f, 1f, 1f);
            newButton.transform.localPosition = Vector3.zero;
            newButton.tag = "ScriptGeneratedButton";

            newButton.GetComponentInChildren<Text>().text = menu[i].name;
            newButton.name = newButton.name + " " + menu[i].name;
            //newButton.GetComponentInChildren<Text>().resizeTextForBestFit = true;

            if (menu[i].name.Length > 7 && menu[i].name.Length < 20)
            {
                newButton.GetComponentInChildren<Text>().fontSize = 18;
            } else if (menu[i].name.Length > 20)
            {
                newButton.GetComponentInChildren<Text>().resizeTextForBestFit = true;
            }

            if (menu[i].hidden)
            {
                newButton.SetActive(false);
            }
            if (menu[i].disabled)
            {
                newButton.GetComponent<Button>().interactable = false;
            }

            int index = i;
            newButton.GetComponent<Button>().onClick.AddListener(delegate { ButtonPress(index, menu[index], newButton.name); });

            // check if network needed
            if ((menu[i].type.EndsWith("Remote") || menu[i].type == "link") /*&& UIController.instance.GetNetworkStatusPanel().GetComponent<NetworkStatusPanel>().GetState() != NetworkStates.ONLINE*/)
            {
                newButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    private void ButtonPress(int buttonIndex, dynamic data)
    {
        ButtonPress(buttonIndex, data, null);
    }
    private void ButtonPress(int buttonIndex, dynamic data, string buttonName)
    {
        //ResetButtonColors();

        switch ((string)data.type)
        {
            case "submenu":
                UIController.instance.HideNonMenuObjects(); //includes ResetButtonColors();
                ColorButtonByName(buttonName);
                PopulateSubMenuButtons(data.subOptions);
                break;
            case "dropdown":
                UIController.instance.HideNonMenuObjects();
                ColorButtonByName(buttonName);
                ToggleDropdown(data.contentOptions, buttonIndex);
                break;
            case "info":
                UIController.instance.HideNonMenuObjects();
                ColorButtonByName(buttonName);
                ToggleInfoPanel(data.contentOptions[0].content, buttonIndex);
                break;
            case "link":
                OpenBrowser(data.contentOptions[0].content.text);
                break;

            case "imageLocal":
                Sprite s = Resources.Load<Sprite>(data.contentOptions[0].content.text);
                //UIController.instance.GetImageRemotePopUp().GetComponent<ImagePopUpControl>().Open(s);
                break;
            case "imageRemote":
                //UIController.instance.GetImageRemotePopUp().GetComponent<ImagePopUpControl>().OpenRemote(data.contentOptions[0].content.text);
                break;
            case "videoLocal":
                //UIController.instance.GetVideoPopUp().GetComponent<VideoControl>().Open(data.contentOptions[0].content.text, false);
                break;
            case "videoRemote":
                //UIController.instance.GetVideoPopUp().GetComponent<VideoControl>().Open(data.contentOptions[0].content.text, true);
                break;
            case "pdfLocal":
                UIController.instance.HideNonMenuObjects();
                ColorButtonByName(buttonName);
                string[] path = data.contentOptions[0].content.text.Split('/');
                UIController.instance.GetPdfPopUp().SetActive(true);
                //UIController.instance.GetPdfPopUp().GetComponent<Paroxe.PdfRenderer.PDFViewer>().LoadDocumentFromResources(path[0], path[1], "", 0);
                break;
            case "pdfRemote":
                UIController.instance.HideNonMenuObjects();
                ColorButtonByName(buttonName);
                UIController.instance.GetPdfPopUp().SetActive(true);
                //UIController.instance.GetPdfPopUp().GetComponent<Paroxe.PdfRenderer.PDFViewer>().LoadDocumentFromWeb(data.contentOptions[0].content.text, "", 0);
                break;
        }
        if ((string)data.type == "submenu")
            LastButtonPressedIndex = -1;
        else
            LastButtonPressedIndex = buttonIndex;
    }

    private void OpenBrowser(string url)
    {
        Debug.Log("Launching web-browser.");
        Application.OpenURL(url);
    }

    private void ToggleDropdown(List<ContentOption> options, int buttonIndex)
    {
        GameObject dd = UIController.instance.GetDropdownPanel();
        Vector3 position = new Vector3((buttonIndex+1) * 142, 0, 0);

        if (LastButtonPressedIndex != buttonIndex)
        {
            //dd.GetComponent<DropdownControl>().PopulateList(options, position);
        } else if (LastButtonPressedIndex == buttonIndex && dd.activeSelf)
        {
            dd.SetActive(false);
        } else
        {
            dd.SetActive(true);
        }
    }
    private void ToggleInfoPanel(Content info, int buttonIndex)
    {
        GameObject cc = UIController.instance.GetContentContainer();

        Vector3 position = new Vector3((buttonIndex + 1) * 142, 0, 0);

        if (LastButtonPressedIndex != buttonIndex)
        {
            //cc.GetComponent<ContentInfoControl>().ShowContentPanel(info, position);
        }
        else if (LastButtonPressedIndex == buttonIndex && cc.activeSelf)
        {
            cc.SetActive(false);
        }
        else
        {
            cc.SetActive(true);
        }
    }
    private void ColorButtonByName(string name)
    {
        Transform btnInMain = null;
        Transform btnInSub = null;
        Button btn = null;

        if (MenuButtonListMain != null)
            btnInMain = MenuButtonListMain.Find(name);

        if (btnInMain != null)
        {
            btn = btnInMain.GetComponent<Button>();
        }
        else if (MenuButtonListSub != null)
        {
            btnInSub = MenuButtonListSub.Find(name);
            if (btnInSub != null)
                btn = btnInSub.GetComponent<Button>();
        }

        if (btn.name == name)
        {
            ColorBlock colors = btn.colors;
            colors.normalColor = Color.blue;
            colors.highlightedColor = Color.cyan;
            colors.pressedColor = Color.cyan;
            btn.colors = colors;
        }
    }
    public void ResetButtonColors()
    {
        if (MenuButtonListMain == null)
            MenuButtonListMain = this.transform.Find("ListContainerMain");

        //skip first button (Button3D) as it has its own script
        for (int i = 1; i < MenuButtonListMain.childCount; i++)
        {
            Transform child = MenuButtonListMain.GetChild(i);

            //Transform btn = MenuButtonListMain.Find(buttonName);
            ColorBlock colors = child.GetComponent<Button>().colors;
            colors.normalColor = Color.white;
            colors.highlightedColor = Color.yellow;
            colors.pressedColor = Color.red;

            child.GetComponent<Button>().colors = colors;
        }

        if (MenuButtonListSub != null)
        {
            //skip first button (HomeButton), it's not visible after press
            for (int i = 1; i < MenuButtonListSub.childCount; i++)
            {
                Transform child = MenuButtonListSub.GetChild(i);

                //Transform btn = MenuButtonListMain.Find(buttonName);
                ColorBlock colors = child.GetComponent<Button>().colors;
                colors.normalColor = Color.white;
                colors.highlightedColor = Color.yellow;
                colors.pressedColor = Color.red;

                child.GetComponent<Button>().colors = colors;
            }
        }
    }
}
