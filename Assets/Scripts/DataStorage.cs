﻿using System;
using System.Collections.Generic;

[Serializable]
public class JsonData
{
    public List<Option> options;
}

[Serializable]
public class Option
{
    public string name;
    public string type;
    public bool hidden;
    public bool disabled;
    public List<SubOption> subOptions;
    public List<ContentOption> contentOptions;
    //public Content content;
}

[Serializable]
public class SubOption
{
    public string name;
    public string type;
    public bool hidden;
    public bool disabled;
    public List<ContentOption> contentOptions;
    //public Content content;
}

[Serializable]
public class ContentOption
{
    public string name;
    public string type;
    public bool hidden;
    public bool disabled;
    public Content content;
}

[Serializable]
public class Content
{
    public string title;
    public string type;
    public string text;
    public List<Media> media;
}

[Serializable]
public class Media
{
    public string text;
    public string type;
    public string path;
}

public enum NetworkStates { CONNECTING, ONLINE, OFFLINE };