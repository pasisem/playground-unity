﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbingInput : MonoBehaviour
{
    [SerializeField]
    private Transform _grabObjParent;

    [SerializeField]
    private bool _leftController;
    private bool _collidesWhileGrabbed;

    private Collider _grabHitbox;
    private GameObject _grabbedObj;

    // Start is called before the first frame update
    void Start()
    {
        _collidesWhileGrabbed = GameSettings.instance.GetGrabbedObjectsCollide();
        if (_grabHitbox == null)
        {
            _grabHitbox = GetComponent<Collider>();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (GrabInputButtonDown())
        //{
        //    _grabbing = true;
        //}

        if (!GrabInputButton() && _grabbedObj != null)
        {
            ObjReleased(_grabbedObj);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
    }

    private void OnTriggerStay(Collider other)
    {
        if (GrabInputButton() && _grabbedObj == null && other.transform.parent != this.transform)
        {
            ObjGrabbed(other);
        }
    }

    void ObjGrabbed(Collider o)
    {
        if (o.tag == "Grabbable Object")
        {
            o.attachedRigidbody.useGravity = false;
            o.attachedRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            o.transform.parent = this.transform;
            _grabbedObj = o.gameObject;
            if (!_collidesWhileGrabbed)
            {
                o.enabled = false;
            }
        }
    }
    void ObjReleased(GameObject obj)
    {
        Collider o = obj.GetComponent<Collider>();
        if (!o.enabled) o.enabled = true;

        o.attachedRigidbody.useGravity = true;
        o.attachedRigidbody.constraints = RigidbodyConstraints.None;
        o.transform.parent = _grabObjParent;
        //o.attachedRigidbody.velocity = _controllerRb.GetRelativePointVelocity(_grabObjParent.position); //todo: add throwing note: commented code is no good for it
        _grabbedObj = null;
    }

    bool GrabInputButton()
    {
        if (_leftController) return InputManager.instance.GetGripLeftButton();
        else return InputManager.instance.GetGripRightButton();
    }
    bool GrabInputButtonUp()
    {
        if (_leftController) return InputManager.instance.GetGripLeftUp();
        else return InputManager.instance.GetGripRightUp();
    }
    bool GrabInputButtonDown()
    {
        if (_leftController) return InputManager.instance.GetGripLeftDown();
        else return InputManager.instance.GetGripRightDown();
    }
}
