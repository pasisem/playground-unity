﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private SmoothSnapLocomotion _smoothLocomotion;
    [SerializeField]
    private CapsuleCollider _playerCollider;
    // Start is called before the first frame update
    void Start()
    {
        _smoothLocomotion = GetComponent<SmoothSnapLocomotion>();
        HeightSettingsUpdated();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MovementSettingsUpdated()
    {
        if (_smoothLocomotion.gameObject.activeInHierarchy)
        {
            _smoothLocomotion.SettingsUpdated();
        }
    }
    public void HeightSettingsUpdated()
    {
        _playerCollider.height = GameSettings.instance.GetPlayerHeight();
    }
}