﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothSnapLocomotion : MonoBehaviour
{
    private float xR, yR, xL, yL = 0f;

    [SerializeField]
    private GameObject _centerCamera;
    [SerializeField]
    private GameObject _leftController;
    private float _movementSpeed = 3f;
    private float _rotationAngle = 45f;
    private bool _leftControllerRelativeMovement = false;

    private float _lastRotation = 0;

    // Start is called before the first frame update
    void Start()
    {
        SettingsUpdated();
    }

    void FixedUpdate()
    {
        Input();
        MovementPlayerRig();
        RotationPlayerRig();
    }

    public void SettingsUpdated()
    {
        _movementSpeed = GameSettings.instance.GetMovementSpeedValue();
        _rotationAngle = GameSettings.instance.GetSnapTurnRadiusValue();
        if (GameSettings.instance.GetMovementRelativeTo() == GameSettings.movementRelativeToOptions.LeftController)
            _leftControllerRelativeMovement = true;
        else
            _leftControllerRelativeMovement = false;
    }

    void Input()
    {
        xR = InputManager.instance.GetAnalogRight().x;
        yR = InputManager.instance.GetAnalogRight().y;
        xL = InputManager.instance.GetAnalogLeft().x;
        yL = InputManager.instance.GetAnalogLeft().y;
    }

    void MovementPlayerRig()
    {
        if (_centerCamera == null)
        {
            Debug.LogError("LowGravLocomotion::MovementPlayerRig: No _centerCamera!");
            return;
        }
        if (_leftController == null)
        {
            Debug.LogError("LowGravLocomotion::MovementPlayerRig: No _leftController!");
            return;
        }

        // By only having Y-axis rotation on the movement relativeTo object the moved object does not have unwanted Y-axis movement
        Transform relativeObjectYRotationOnly;
        if (_leftControllerRelativeMovement)
        {
            relativeObjectYRotationOnly = _leftController.transform;
        }
        else
        {
            relativeObjectYRotationOnly = _centerCamera.transform;
        }
        relativeObjectYRotationOnly.SetPositionAndRotation(relativeObjectYRotationOnly.position, Quaternion.Euler(0f, relativeObjectYRotationOnly.rotation.eulerAngles.y, 0f));

        transform.Translate(
            new Vector3(
                xL,
                0f,
                yL
            ) * Time.deltaTime * _movementSpeed, relativeObjectYRotationOnly
        );

    }

    void RotationPlayerRig()
    {
        float nextRotation = 0;
        if (xR > 0.1)
        {
            nextRotation = 1;
        }
        else if (xR < -0.1)
        {
            nextRotation = -1;
        }
        else
        {
            _lastRotation = 0;
        }

        if (_lastRotation != nextRotation)
        {
            transform.SetPositionAndRotation(transform.position, Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + _rotationAngle * nextRotation, transform.rotation.eulerAngles.z));
            _lastRotation = nextRotation;
        }
    }
}
