﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeletePlayerPrefs : MonoBehaviour {
    public bool deleteOnStartAllowed;

	// Use this for initialization
	void Start () {
        if (deleteOnStartAllowed)
        {
            PlayerPrefs.DeleteAll();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
