﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettingsUIManager : MonoBehaviour
{
    [SerializeField]
    private TMPro.TMP_Dropdown _snapTurnRadius;
    [SerializeField]
    private TMPro.TMP_Dropdown _movementRelativeTo;
    [SerializeField]
    private TMPro.TMP_Dropdown _movementSpeed;
    [SerializeField]
    private TMPro.TextMeshProUGUI _playerHeightText;

    private float _playerHeightValue;
    private readonly float _playerHeightChange = 0.1f;
    private float _playerHeightMax;
    private float _playerHeightMin;

    void Start()
    {
        if (_snapTurnRadius == null)
        {
            Debug.LogError("GameSettingsUIManager::Start: No _snapTurnRadius!");
            return;
        }

        if (_movementRelativeTo == null)
        {
            Debug.LogError("GameSettingsUIManager::Start: No _movementRelativeTo!");
            return;
        }

        if (_movementSpeed == null)
        {
            Debug.LogError("GameSettingsUIManager::Start: No _movementSpeed!");
            return;
        }

        if (_playerHeightText == null)
        {
            Debug.LogError("GameSettingsUIManager::Start: No _playerHeightText!");
            return;
        }
    }


    public void InitValues()
    {
        _playerHeightMin = GameSettings.instance.minPlayerHeight;
        _playerHeightMax = GameSettings.instance.maxPlayerHeight;

        _snapTurnRadius.options.Clear();
        _movementRelativeTo.options.Clear();
        _movementSpeed.options.Clear();

        foreach (var turnOption in Enum.GetNames(typeof(GameSettings.snapTurnRadiusOptions)))
        {
            _snapTurnRadius.options.Add(new TMPro.TMP_Dropdown.OptionData(turnOption));
        }
        foreach (string relativeToOption in Enum.GetNames(typeof(GameSettings.movementRelativeToOptions)))
        {
            _movementRelativeTo.options.Add(new TMPro.TMP_Dropdown.OptionData(relativeToOption));
        }
        foreach (var speedOption in Enum.GetNames(typeof(GameSettings.movementSpeedOptions)))
        {
            _movementSpeed.options.Add(new TMPro.TMP_Dropdown.OptionData(speedOption));
        }


        this.UpdateSnapTurnRadius();
        this.UpdateMovementRelativeTo();
        this.UpdateMovementSpeed();
        this.UpdatePlayerHeight();
    }
    public void UpdateSnapTurnRadius()
    {
        _snapTurnRadius.value = (int)GameSettings.instance.GetSnapTurnRadius();
    }
    public void ChangeSnapTurnRadius()
    {
        GameSettings.instance.SetSnapTurnRadius((GameSettings.snapTurnRadiusOptions)_snapTurnRadius.value);
        this.UpdateSnapTurnRadius();
    }

    public void UpdateMovementSpeed()
    {
        _movementSpeed.value = (int)GameSettings.instance.GetMovementSpeed();
    }
    public void ChangeMovementSpeed()
    {
        GameSettings.instance.SetMovementSpeed(_movementSpeed.value);
    }
    private void UpdatePlayerHeightText()
    {
        _playerHeightText.text = _playerHeightValue.ToString("0.0") + " m";
    }
    public void UpdatePlayerHeight()
    {
        _playerHeightValue = GameSettings.instance.GetPlayerHeight();
        this.UpdatePlayerHeightText();
    }
    public void ChangePlayerHeightPlus()
    {
        float newH = _playerHeightValue + _playerHeightChange;
        if (_playerHeightMax > newH)
        {
            GameSettings.instance.SetPlayerHeight(newH);
            this.UpdatePlayerHeight();
        }
    }
    public void ChangePlayerHeightMinus()
    {
        float newH = _playerHeightValue - _playerHeightChange;
        if (_playerHeightMin < newH)
        {
            GameSettings.instance.SetPlayerHeight(newH);
            this.UpdatePlayerHeight();
        }
    }
    public void UpdateMovementRelativeTo()
    {
        _movementRelativeTo.value = (int)GameSettings.instance.GetMovementRelativeTo();
    }
    public void ChangeMovementRelativeTo()
    {
        GameSettings.instance.SetMovementRelativeTo((GameSettings.movementRelativeToOptions)_movementRelativeTo.value);
        this.UpdateMovementRelativeTo();
    }
}
