﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    enum InputAPI
    {
        Oculus,
        Steam,
        WMR
    };

    private InputAPI _activeAPI;


    public TMPro.TextMeshProUGUI inputTextLeft;
    public TMPro.TextMeshProUGUI inputTextRight;
    private StringBuilder sbLeftController, sbRightController;
    // Start is called before the first frame update
    void Start()
    {
        _activeAPI = InputAPI.Oculus;
        sbLeftController = new StringBuilder();
        sbRightController = new StringBuilder();
    }

    // Update is called once per frame
    void Update()
    {
        TextUpdate();
    }

    void TextUpdate()
    {
        sbLeftController.Clear();
        sbRightController.Clear();

        sbLeftController.AppendLine("Trigger: " + GetTriggerLeft().ToString("0.0"));
        sbLeftController.AppendLine("Grip: " + GetGripLeftValue().ToString("0.0"));
        sbLeftController.AppendLine("Analog: " + GetAnalogLeft());
        sbLeftController.AppendLine("Stick: " + GetStickLeft());
        sbLeftController.AppendLine("Y: " + GetY());
        sbLeftController.AppendLine("X: " + GetX());

        sbRightController.AppendLine("Trigger: " + GetTriggerRight().ToString("0.0"));
        sbRightController.AppendLine("Grip: " + GetGripRightValue().ToString("0.0"));
        sbRightController.AppendLine("Analog: " + GetAnalogRight());
        sbRightController.AppendLine("Stick: " + GetStickRight());
        sbRightController.AppendLine("B: " + GetB());
        sbRightController.AppendLine("A: " + GetA());

        inputTextLeft.text = sbLeftController.ToString();
        inputTextRight.text = sbRightController.ToString();
    }

    public Vector2 GetAnalogLeft()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);
        }
        else
        {
            return new Vector2();
        }
    }
    public Vector2 GetAnalogRight()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
        }
        else
        {
            return new Vector2();
        }
    }

    public float GetTriggerLeft()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawAxis1D.LIndexTrigger);
        }
        else
        {
            return 0f;
        }
    }
    public bool GetTriggerLeftUp()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.LIndexTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetTriggerLeftDown()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.LIndexTrigger);
        }
        else
        {
            return false;
        }
    }
    public float GetTriggerRight()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger);
        }
        else
        {
            return 0f;
        }
    }
    public bool GetTriggerRightUp()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.RIndexTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetTriggerRightDown()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger);
        }
        else
        {
            return false;
        }
    }
    public float GetGripLeftValue()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawAxis1D.LHandTrigger);
        }
        else
        {
            return 0f;
        }
    }
    public bool GetGripLeftButton()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.LHandTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetGripLeftUp()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.LHandTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetGripLeftDown()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.LHandTrigger);
        }
        else
        {
            return false;
        }
    }
    public float GetGripRightValue()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger);
        }
        else
        {
            return 0f;
        }
    }
    public bool GetGripRightButton()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.RHandTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetGripRightUp()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.RHandTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetGripRightDown()
    {

        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.RHandTrigger);
        }
        else
        {
            return false;
        }
    }
    public bool GetA()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.A);
        }
        else
        {
            return false;
        }
    }
    public bool GetAUp()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.A);
        }
        else
        {
            return false;
        }
    }
    public bool GetADown()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.A);
        }
        else
        {
            return false;
        }
    }
    public bool GetB()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.B);
        }
        else
        {
            return false;
        }
    }
    public bool GetBUp()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.B);
        }
        else
        {
            return false;
        }
    }
    public bool GetBDown()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.B);
        }
        else
        {
            return false;
        }
    }

    public bool GetX()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.X);
        }
        else
        {
            return false;
        }
    }
    public bool GetXUp()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.X);
        }
        else
        {
            return false;
        }
    }
    public bool GetXDown()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.X);
        }
        else
        {
            return false;
        }
    }
    public bool GetY()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.Y);
        }
        else
        {
            return false;
        }
    }
    public bool GetYUp()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.Y);
        }
        else
        {
            return false;
        }
    }
    public bool GetYDown()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.Y);
        }
        else
        {
            return false;
        }
    }
    public bool GetStickLeft()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.LThumbstick);
        }
        else
        {
            return false;
        }
    }
    public bool GetStickLeftUp()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.LThumbstick);
        }
        else
        {
            return false;
        }
    }
    public bool GetStickLeftDown()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.LThumbstick);
        }
        else
        {
            return false;
        }
    }
    public bool GetStickRight()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.Get(OVRInput.RawButton.RThumbstick);
        }
        else
        {
            return false;
        }
    }
    public bool GetStickRightUp()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetUp(OVRInput.RawButton.RThumbstick);
        }
        else
        {
            return false;
        }
    }
    public bool GetStickRightDown()
    {
        if (_activeAPI == InputAPI.Oculus)
        {
            return OVRInput.GetDown(OVRInput.RawButton.RThumbstick);
        }
        else
        {
            return false;
        }
    }





    public static InputManager instance;
    private void Awake()
    {
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
