﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    enum ActiveControllerOptions
    {
        Left, Right, Both
    }
    public enum movementSpeedOptions
    {
        VerySlow,
        Slow,
        Medium,
        Fast,
        VeryFast
    }
    public enum movementRelativeToOptions
    {
        Head,
        LeftController
    }
    public enum snapTurnRadiusOptions
    {
        Fifteen,
        Thirty,
        FourtyFive,
        Sixty,
        Ninety
    }

    public readonly float minPlayerHeight = 0.5f;
    public readonly float maxPlayerHeight = 10f;
    public GameSettingsUIManager settingsUI;
    public PlayerManager playerManager;

    [SerializeField]
    private float[] _movementSpeedValues = { 1, 3, 5, 10, 100 };

    [SerializeField]
    private float[] _snapTurnRadiusValues = { 15, 30, 45, 60, 90 };

    [SerializeField]
    private movementRelativeToOptions movementRelativeTo; // if false, movement is relativeTo Head Mounted Display (Center Camera)

    [SerializeField]
    [Range(0.3f, 5f)]
    private float playerHeight; // localAvatar pill collider height

    [SerializeField]
    private snapTurnRadiusOptions snapTurnRadius;

    [SerializeField]
    private movementSpeedOptions movementSpeed;

    [SerializeField]
    private bool _grabbedObjectsCollide;

    void Start()
    {
        if (settingsUI == null)
            Debug.LogError("GameSettings::Start: No settingsUI!");
        if (playerManager == null)
            Debug.LogError("GameSettings::Start: No playerManager!");

        settingsUI.InitValues();
    }

    void GetPlayerPrefs()
    {

    }

    void SetPlayerPrefs()
    {

    }

    public movementRelativeToOptions GetMovementRelativeTo()
    {
        return this.movementRelativeTo;
    }
    public void SetMovementRelativeTo(movementRelativeToOptions o)
    {
        this.movementRelativeTo = o;
        playerManager.MovementSettingsUpdated();
    }
    public float GetPlayerHeight()
    {
        return this.playerHeight;
    }
    public void SetPlayerHeight(float h)
    {
        if (h >= this.minPlayerHeight && h <= this.maxPlayerHeight)
        {
            this.playerHeight = h;
            playerManager.HeightSettingsUpdated();
        }
        else
        {
            Debug.LogError("GameSettings::SetPlayerHeight: Attempted to set PlayerHeight out of range (0.5-10).");
        }
    }
    public movementSpeedOptions GetMovementSpeed()
    {
        return this.movementSpeed;
    }
    public float GetMovementSpeedValue()
    {
        return _movementSpeedValues[(int)this.movementSpeed];
    }
    public void SetMovementSpeed(int index)
    {
        this.movementSpeed = (movementSpeedOptions)index;
        playerManager.MovementSettingsUpdated();
    }
    public float GetSnapTurnRadiusValue()
    {
        return _snapTurnRadiusValues[(int)this.snapTurnRadius];
    }
    public snapTurnRadiusOptions GetSnapTurnRadius()
    {
        return this.snapTurnRadius;
    }
    public void SetSnapTurnRadius(snapTurnRadiusOptions r)
    {
        this.snapTurnRadius = r;
        playerManager.MovementSettingsUpdated();
    }

    public bool GetGrabbedObjectsCollide()
    {
        return _grabbedObjectsCollide;
    }

    public static GameSettings instance;
    private void Awake()
    {
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
