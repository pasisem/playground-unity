﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRPointerControllerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _controllerLeft;
    [SerializeField]
    private GameObject _controllerRight;

    private bool _activeControllerLeft;

    // Start is called before the first frame update
    void Start()
    {
        if (_controllerLeft == null && _controllerRight == null)
        {
            Debug.LogError("VRPointerControllerManager::Start: No controllers!");
            this.gameObject.SetActive(false);
        }
        if (_controllerLeft == null) _activeControllerLeft = false;
        else if (_controllerRight == null) _activeControllerLeft = true;

        if (_activeControllerLeft) SetPointerControllerLeft();
        else SetPointerControllerRight();
    }

    void FixedUpdate()
    {
        if (!_activeControllerLeft && InputManager.instance.GetTriggerLeftDown())
        {
            SetPointerControllerLeft();
        }
        else if (_activeControllerLeft && InputManager.instance.GetTriggerRightDown())
        {
            SetPointerControllerRight();
        }
    }

    void SetPointerControllerLeft()
    {
        _activeControllerLeft = true;
        this.transform.parent = _controllerLeft.transform;
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

    void SetPointerControllerRight()
    {
        _activeControllerLeft = false;
        this.transform.parent = _controllerRight.transform;
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public bool GetActiveControllerTriggerUp()
    {
        if (_activeControllerLeft)
        {
            return InputManager.instance.GetTriggerLeftUp();
        }
        else
        {
            return InputManager.instance.GetTriggerRightUp();
        }
    }
    public bool GetActiveControllerTriggerDown()
    {
        if (_activeControllerLeft)
        {
            return InputManager.instance.GetTriggerLeftDown();
        }
        else
        {
            return InputManager.instance.GetTriggerRightDown();
        }
    }
}
