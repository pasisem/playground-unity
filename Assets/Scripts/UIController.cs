﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController: MonoBehaviour {
    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] private GameObject DropdownPanel;
    [SerializeField] private GameObject ContentContainer;
    [SerializeField] private GameObject ImagePopUp;
    [SerializeField] private GameObject ImageRemotePopUp;
    [SerializeField] private GameObject VideoPopUp;
    [SerializeField] private GameObject PdfPopUp;
    [SerializeField] private GameObject TimerCircle;
    [SerializeField] private GameObject SpinnerMedia;
    [SerializeField] private GameObject NetworkStatusPanel;

    public void InitUI(List<Option> o)
    {
        // init Menu Buttons
        MainMenuPanel.GetComponent<MainMenuControl>().PopulateMenuButtons(o);
    }

    public GameObject GetDropdownPanel()
    {
        return DropdownPanel;
    }

    public GameObject GetContentContainer()
    {
        return ContentContainer;
    }

    public GameObject GetImagePopUp()
    {
        return ImagePopUp;
    }
    
    public GameObject GetImageRemotePopUp()
    {
        return ImageRemotePopUp;
    }

    public GameObject GetVideoPopUp()
    {
        return VideoPopUp;
    }

    public GameObject GetPdfPopUp()
    {
        return PdfPopUp;
    }

    public GameObject GetTimerCircle()
    {
        return TimerCircle;
    }

    public GameObject GetSpinnerMedia()
    {
        return SpinnerMedia;
    }

    public GameObject GetNetworkStatusPanel()
    {
        return NetworkStatusPanel;
    }

    public void HideNonMenuObjects()
    {
        DropdownPanel.SetActive(false);
        ContentContainer.SetActive(false);
        ImagePopUp.SetActive(false);
        ImageRemotePopUp.SetActive(false);
        PdfPopUp.SetActive(false);
        //VideoPopUp.SetActive(false);
        SpinnerMedia.SetActive(false);
        //VideoPopUp.GetComponent<VideoControl>().Close();
        MainMenuPanel.GetComponent<MainMenuControl>().ResetButtonColors();
    }

    public static UIController instance;
    private void Awake()
    {
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
